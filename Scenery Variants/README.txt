If you are using an AIRAC cycle from June 2021 or later you need to install this to allow various addon aircraft to recognize the new runway configuration at RKSI. 

To install, copy the Earth nav data folder from this directory into your main RKSI scenery folder and overwrite all files. You will need to reload the sim (reloading the scenery without restarting the sim may cause it to crash or not reload correctly). 

This adds a new "ghost" runway. This only modifies the xplane metadata to recognize the new configuration, the runway will still physically not be there. This is expected to be updated at some point in the future, however at the time of release there is not enough information for us to implement the runway accurately. There is no date for this update currently. 