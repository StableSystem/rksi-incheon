A
800
FOREST

TEXTURE Grass.png
NO_SHADOW
NO_BLEND

LOD 200

SCALE_X 4096
SCALE_Y 4096

SPACING 1.5 1.5

RANDOM	0.3	0.3

#low-left coord tex size center percentage    ----height----
#tree  s 	t 	w 	y 	offset occurrence    	min 	 max  	quads 	type 	name
#---------------------------------------------------------------------------------------------------
#TREE	974.998	6.85279	1018.62	163.384	533.683	100	1.0	1.1	2	1	Grass
TREE	2109 2241 1070 616 2644 100 0.7 0.9 2 1 Grass Green Short
