# RKSI - Incheon International by Zero Dollar Payware


**[OFFICIAL FORUM THREAD](https://forums.x-plane.org/index.php?/forums/topic/231209-rksi-incheon-intl-by-zero-dollar-payware/&page=2&tab=comments#comment-2164747)**

**[Official ZDP Discord](https://discord.gg/YADGVW8zWv)**

**Installation Instructions:**

Download and move the unzipped rksi-incheon folder into your custom scenery folder

Use xOrganizer, or delete your scenery_packs.ini to adjust your scenery load order if using our provided ortho. If you have issues with the scenery loading, most likely your load order is wrong.

**Required Libraries**

*MisterX Library V2.0+

*SAM Library V2+

Please report any bugs on the official project discord or on the official forum thread (linked above)

To conctact us about this repo please send a message on [the org](https://forums.x-plane.org/index.php?/profile/534962-stablesystem/) or on discord

This repository and it's contents are protected under [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/)
Assets used from other developers was done so with their knowledge and approval. 

**Credits**

ZDP Developers: StableSystem, Martini, Imran, awfsiu, TJ

Additional Considerations to the following people

YSLaurens#2763

르밴#0584

Tino

MisterX

Pyreegue

World Imagery (orthophotos)

Esri. "Imagery" [basemap]. Scale Not Given. "World Imagery". January 21, 2021. https://www.arcgis.com/home/item.html?id=10df2279f9684e4a9f6a7f08febac2a9. (February 18, 2021).

**Open Source Resources**

This scenery is entirely open source and I encourage people to look through our source if you are curious how something is done or want to make modifications. If you modify something and think it should be implemented, please send me a message and I'd be happy to merge it into the official release. 

RKSI source - https://gitlab.com/StableSystem/rksi-incheon
